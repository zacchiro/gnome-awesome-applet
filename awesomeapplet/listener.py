#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GNOME Awesome Applet - control Awesome window manager via GNOME panel
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.


import dbus
import dbus.service
import logging


DBUS_IFACE = "cc.upsilon.awesomeapplet.NotifyInterface"
DBUS_NAME = 'cc.upsilon.awesomeapplet'
DBUS_PATH = '/cc/upsilon/awesomeapplet'

class AwesomeListener(dbus.service.Object):
    """listen for events coming via DBus

    An AwesomeListener will listen for two different kinds of events: 1) DBus
    signals (e.g. LayoutUpdate) and 2) method invocations (e.g. LuaPrompt).
    Signals are delivered via interface DBUS_IFACE; methods invocations happen
    on bus name DBUS_NAME on object at DBUS_PATH.

    Several AwesomeListener-s will compete for method invocations (but not for
    signals): only the first instance will receive method invocations. Upon
    shutdown, other instances will transparently take over. This characteristic
    is used to insure that suitable methods (e.g. Lua prompt) are executed only
    once, instead of being executed by all applet instances.
    """

    def __init__(self):
        logging.debug('AwesomeListener.__init__')
        self.__controller = None
        # signals
        bus = dbus.SessionBus()
        bus.add_signal_receiver(lambda: self.update_layout(),
                                dbus_interface=DBUS_IFACE,
                                signal_name="LayoutUpdate")
        # method invocations
        bus_name = dbus.service.BusName(DBUS_NAME, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, DBUS_PATH)
        logging.debug('AwesomeListener.__init__')
 
    def set_controller(self, awesome):
        self.__controller = awesome

    def update_layout(self):
        logging.debug('AwesomeListener.update_layout')
        if self.__controller:
            self.__controller.update_status()
        logging.debug('/AwesomeListener.update_layout')

    @dbus.service.method(DBUS_NAME)
    def LuaPrompt(self):
        logging.debug('/AwesomeListener.LuaPrompt')
        self.__controller.lua_prompt()
        logging.debug('/AwesomeListener.LuaPrompt')
