#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GNOME Awesome Applet - control Awesome window manager via GNOME panel
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import pygtk
pygtk.require('2.0')
import dbus
import dbus.service
import gtk
import gtk.glade as glade
import logging
import os.path

import config
from client import AwesomeClient
from util import fd_notify


class AwesomeController():
    
    def __init__(self, applet, icon):
        logging.debug('AwesomeController.__init__')
        self.__applet = applet
        self.__icon = icon
        self.__client = AwesomeClient()
        self.update_status()

        self.__pref = glade.XML(config.GLADE_PATH, 'preferences_dialog' \
                                    ).get_widget('preferences_dialog')
        self.__about = glade.XML(config.GLADE_PATH, 'about_dialog' \
                                    ).get_widget('about_dialog')
        self.__prompt_g = glade.XML(config.GLADE_PATH, 'prompt_dialog')
        self.__prompt = self.__prompt_g.get_widget('prompt_dialog')
        self.__prompt_text = self.__prompt_g.get_widget('lua_prompt_text')

        self.__about.set_logo(gtk.gdk.pixbuf_new_from_file(config.ICON_PATH))
        self.__about.set_version(config.VERSION)
        self.__prompt.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK).grab_default()
        self.__prompt.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
        self._connect_signals()

        self.__applet.setup_menu_from_file(
            None, config.MENU_PATH, None,
            [#('Preferences', lambda c,v: self.__pref.show()),
             ('Lua Prompt', lambda c,v: self.lua_prompt()),
             ('About', lambda c,v: self.__about.show())])
        logging.debug('/AwesomeController.__init__')

    def update_status(self):
        logging.debug('AwesomeController.update_status')
        self.__icon.set_layout(self.__client.get_layout_name(),
                               self.__applet.get_size())
        logging.debug('/AwesomeController.update_status')

    def lua_prompt(self):
        logging.debug('AwesomeController.lua_prompt')
        self.__prompt_text.select_region(0, -1)
        self.__prompt_text.grab_focus()
        self.__prompt.show()
        logging.debug('/AwesomeController.lua_prompt')

    def _connect_signals(self):
        # applet
        self.__applet.connect('button-press-event', self._on_click)
        self.__applet.connect('change-orient', self.update_status)
        self.__applet.connect('change-size', self.update_status)
        self.__applet.connect('destroy', self._on_destroy)
        # about dialog
        self.__about.connect('response', lambda c,v: self.__about.hide())
        self.__about.connect('delete-event', self.__about.hide_on_delete)
        # preferences dialog
        self.__pref.connect('response', lambda c,v: self.__pref.hide())
        self.__pref.connect('delete-event', self.__pref.hide_on_delete)
        # Lua prompt dialog
        self.__prompt.connect('response', self._on_lua_prompt_response)
        self.__prompt.connect('delete-event', self.__prompt.hide_on_delete)

    def _on_destroy(self, sender):
        pass

    def _on_click(self, widget, event):
        if event.button == 1:
            self.__client.next_layout()
            self.update_status()
        return False

    def _on_lua_prompt_response(self, dialog, response):
        if response == gtk.RESPONSE_OK:
            ret = self.__client.eval(self.__prompt_text.get_text())
            if ret is not None:
                fd_notify(str(ret), title='Lua result:')
        elif response == gtk.RESPONSE_CANCEL:
            pass
        dialog.hide()


class AwesomeIcon(gtk.Image):
    tooltip_template = "Awesome layout: %s"

    def __init__(self):
        gtk.Image.__init__(self)

    def set_layout(self, layout, size):
        layout_icon = os.path.join(config.LAYOUT_DIR, layout + '.png')
        if os.path.isfile(layout_icon):
            buf = gtk.gdk.pixbuf_new_from_file_at_size(layout_icon, size, size)
            self.set_from_pixbuf(buf)
            self.set_tooltip_text(self.tooltip_template % layout)
