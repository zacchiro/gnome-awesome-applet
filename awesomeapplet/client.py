#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GNOME Awesome Applet - control Awesome window manager via GNOME panel
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

"""client interface to a running Awesome instance"""

import dbus
import logging
from subprocess import Popen, PIPE


class _DBusClient():
    """Awesome client, communicating via DBus"""

    # see /usr/bin/awesome-client for reference
    DBUS_DEST = 'org.naquadah.awesome.awful'
    DBUS_PATH = '/'
    DBUS_IFACE = DBUS_DEST + '.Remote'

    def __init__(self):
        logging.debug('_DBusClient.__init__')
        self.__proxy = self._get_awesome_proxy()
        logging.debug('/_DBusClient.__init__')

    def _get_awesome_proxy(self):
        logging.debug('_DBusClient._get_awesome_proxy')
        bus = dbus.SessionBus()
        # Do not introspect to avoid timeouts, as Awesome DBus object does not
        # advertise an interface.
        obj = bus.get_object(self.DBUS_DEST, self.DBUS_PATH, introspect=False)
        iface = dbus.Interface(obj, dbus_interface=self.DBUS_IFACE)
        logging.debug('/_DBusClient._get_awesome_proxy')
        return iface

    def _eval(self, cmd):
        logging.debug('_DBusClient._eval')
        try:
            ret = self.__proxy.Eval(cmd)
        except dbus.DBusException, exn:
            if exn.get_dbus_name() == 'org.freedesktop.DBus.Error.ServiceUnknown':
                # try (once) to re-obtain awesome object proxy
                # this is needed, for instance, upon Awesome restart
                self.__proxy = self._get_awesome_proxy()
                ret = self.__proxy.Eval(cmd)
            else:
                raise
        logging.debug('/_DBusClient._eval')
        return ret


class _ClientMixin(object):
    """High-level methods to fetch/set Awesome information"""

    def get_layout_name(self):
        """return curren layout name"""
        logging.debug('_ClientMixin.get_layout_name')
        ret = self._eval('return awful.layout.getname()')
        logging.debug('/_ClientMixin.get_layout_name')
        return ret

    def next_layout(self):
        logging.debug('_ClientMixin.next_layout')
        """switch to next layout"""
        ret = self._eval('awful.layout.inc(layouts,  1)')
        logging.debug('/_ClientMixin.next_layout')
        return ret

    def eval(self, lua_code):
        logging.debug('_ClientMixin.eval: ' + lua_code)
        ret = self._eval(lua_code)
        logging.debug('/_ClientMixin.eval')
        return ret


class AwesomeClient(_DBusClient, _ClientMixin):
    """Awesome client, interacting with a running Awesome instance
    """
    pass
