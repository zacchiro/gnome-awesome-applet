#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GNOME Awesome Applet - control Awesome window manager via GNOME panel
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus
import logging

import config

NOTIFY_DBUS_NAME = 'org.freedesktop.Notifications'
NOTIFY_DBUS_PATH = '/org/freedesktop/Notifications'
NOTIFY_DBUS_IFACE = 'org.freedesktop.Notifications'


def fd_notify(msg, title=''):
    "deliver a message to the user via Freedesktop notification facility"
    logging.debug('util.fd_notify: ' + msg)

    app_name = 'GNOME Awesome Applet'
    array = ''
    hint = ''
    icon = config.ICON_PATH
    time = 2000	# milliseconds

    bus = dbus.SessionBus()
    obj = bus.get_object(NOTIFY_DBUS_NAME, NOTIFY_DBUS_PATH)
    proxy = dbus.Interface(obj, NOTIFY_DBUS_IFACE)
    proxy.Notify(app_name, 0, icon, title, msg, array, hint, time)
    logging.debug('/util.fd_notify')

