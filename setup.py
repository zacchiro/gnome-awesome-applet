#!/usr/bin/env python

from distutils.core import setup

from awesomeapplet import config

setup(name=config.PACKAGE,
      version=config.VERSION,
      description='GNOME Awesome Applet',
      long_description='GNOME panel applet to interfact with the Awesome window manager',
      url='http://upsilon.cc/~zack/hacking/software/gnome-awesome-applet/',
      download_url='http://git.upsilon.cc/cgi-bin/gitweb.cgi?p=gnome-awesome-applet.git',
      license='GNU General Public License, version 3 or above',
      packages=['awesomeapplet'],
      data_files=[(config.BIN_DIR, ['gnome-awesome-applet']),
                  (config.BONOBO_DIR, ['AwesomeApplet.server']),
                  (config.PIXMAPS_DIR, ['images/gnome-awesome-applet.png']),
                  (config.RESOURCES_DIR, ['data/awesome-applet.glade',
                                          'data/AwesomeApplet.xml']),
                  (config.LUA_DIR, ['lua/gnome-applet.lua'])
                  ],
      requires=['dbus','gtk', 'gtk.glade', 'gnomeapplet']
      )
