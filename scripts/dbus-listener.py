#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GNOME Awesome Applet - control Awesome window manager via GNOME panel
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# test with, e.g.:
#   $ dbus-send --type=signal / cc.upsilon.awesomeapplet.NotifyInterface.LayoutUpdate

import gtk
import dbus

from dbus.mainloop.glib import DBusGMainLoop


class AwesomeListener(object):

    DBUS_IFACE = "cc.upsilon.awesomeapplet.NotifyInterface"

    def __init__(self):
        bus = dbus.SessionBus()
        bus.add_signal_receiver(lambda: self.update_layout(),
                                dbus_interface=self.DBUS_IFACE,
                                signal_name="LayoutUpdate")
        print 'receiver set up, waiting for signals...'
 
    def update_layout(self):
        print 'received notice: LayoutUpdate'


if __name__ == '__main__':
    DBusGMainLoop(set_as_default=True)
    listener = AwesomeListener()
    gtk.main()
