#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# GNOME Awesome Applet - control Awesome window manager via GNOME panel
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

# test with, e.g.:
#   $ dbus-send --dest=cc.upsilon.awesomeapplet --type=method_call --print-reply /cc/upsilon/awesomeapplet cc.upsilon.awesomeapplet.LuaPrompt

import gtk
import dbus
import dbus.service

from dbus.mainloop.glib import DBusGMainLoop


DBUS_NAME = 'cc.upsilon.awesomeapplet'
DBUS_PATH = '/cc/upsilon/awesomeapplet'

class AwesomeObject(dbus.service.Object):
    """register a DBus object %s at DBus name %s, reacting to methods via DBus

    provide mutual-exclusion if multiple objects are registered: only the first
    one will react, with fallback to others upon close (i.e. native DBus
    behaviour for methods).
    """

    def __init__(self):
        bus_name = dbus.service.BusName(DBUS_NAME, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, DBUS_PATH)
        print 'service setup, waiting for invocations...'
 
    @dbus.service.method(DBUS_NAME)
    def LuaPrompt(self):
        print 'LuaPrompt'

if __name__ == '__main__':
    DBusGMainLoop(set_as_default=True)
    listener = AwesomeObject()
    gtk.main()
