#!/usr/bin/python

import gtk
import dbus
import dbus.service

from dbus.mainloop.glib import DBusGMainLoop


DBUS_IFACE = "cc.upsilon.awesomeapplet.NotifyInterface"
DBUS_NAME = 'cc.upsilon.awesomeapplet'
DBUS_PATH = '/cc/upsilon/awesomeapplet'

class AwesomeListener(dbus.service.Object):
    def __init__(self):
        self.__controller = None
        # signals
        bus = dbus.SessionBus()
        bus.add_signal_receiver(lambda: self.update_layout(),
                                dbus_interface=DBUS_IFACE,
                                signal_name="LayoutUpdate")
        # method invocations
        bus_name = dbus.service.BusName(DBUS_NAME, bus=dbus.SessionBus())
        dbus.service.Object.__init__(self, bus_name, DBUS_PATH)
        print 'initialization completed, waiting for signals/invocations...'
 
    @dbus.service.method(DBUS_NAME)
    def LuaPrompt(self):
        print 'LuaPrompt'

    def update_layout(self):
        print 'update_layout'


if __name__ == '__main__':
    DBusGMainLoop(set_as_default=True)
    listener = AwesomeListener()
    gtk.main()

