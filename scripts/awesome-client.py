#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# awesome-client.py - Python version of awesome-client, for testing purposes
#
# Copyright © 2010 Stefano Zacchiroli <zack@upsilon.cc>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.

import dbus
import sys

DBUS_DEST = 'org.naquadah.awesome.awful'
DBUS_PATH = '/'
DBUS_IFACE = DBUS_DEST + '.Remote'

bus = dbus.SessionBus()
awesome = bus.get_object(DBUS_DEST, DBUS_PATH, introspect=False)
iface = dbus.Interface(awesome, dbus_interface=DBUS_IFACE)

if __name__ == '__main__':
    while True:
        line = sys.stdin.readline()
        if not line:
            break
        ret = iface.Eval(line.rstrip('\n'))
        print 'response type:', type(ret)
        print ret
